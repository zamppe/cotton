use map::Map;
use components::{Tile, TileType};

use constants::MAP_TILES;

use petgraph::Undirected;
use petgraph::graph::Graph;
use petgraph::algo::astar;
use petgraph::graph::NodeIndex;

pub fn map_to_graph(map: &Map) -> Graph<Tile, u32, Undirected> {
    fn index_of_tile_in_graph(tile: Tile, graph: &Graph<Tile, u32, Undirected>) -> Option<NodeIndex> {
        for ix in graph.node_indices() {
            let tile_in_graph = *graph.node_weight(ix).unwrap();
            if  tile_in_graph == tile {
                return Some(ix);
            }
        }

        None
    }

    let mut g = Graph::new_undirected();

    for y in 0..MAP_TILES as i32 {
        for x in 0..MAP_TILES as i32 {
            if let Some(tile) = map.tile_at(x, y) {                
                match tile.tile_type {
                    TileType::Space | TileType::Start | TileType::End => {
                        g.add_node(*tile);
                    },
                    TileType::Solid => {},
                }
            }

        }
    }

    for ixa in g.node_indices() {
        let tile = *g.node_weight(ixa).unwrap();

        let neighbors = map.tile_neighbors(tile);

        for neighbor in neighbors {
            let ixb = index_of_tile_in_graph(neighbor, &g);

            if let Some(ixb) = ixb {
                g.update_edge(ixa, ixb, 1);
            }
        }
    }

    g
}

pub fn graph_to_path(graph: &Graph<Tile, u32, Undirected>) -> Vec<Tile> {
    fn start(graph: &Graph<Tile, u32, Undirected>) -> Option<NodeIndex> {
        for ix in graph.node_indices() {
            let tile = &graph.node_weight(ix).unwrap();

            match tile.tile_type {                
                TileType::Start => return Some(ix),
                _ => {},
            }
        }

        return None;
    }

    let (_cost, astarpath) = astar(&graph,
        start(&graph).unwrap(),
        |finish| graph.node_weight(finish).unwrap().is_end(),
        |e| *e.weight(),
        |_| 0).unwrap();

    let mut path: Vec<Tile> = Vec::new();
    for node in astarpath {
        let tile = *graph.node_weight(node).unwrap();
        path.push(tile);
    }

    path
}
