use components::*;
use constants::*;
use std::collections::HashMap;

use std::fs;

#[derive(Clone)]
pub struct Map {
    pub tiles: HashMap<(i32, i32), Tile>,
}

impl Map {
    fn validate_map_raw_data(data: &String) {
        let rows = data.split('\n')
            .filter(|row| row.len() > 0);

        let row_max_len = rows.clone().max_by(|x, y| x.len().cmp(&y.len())).unwrap().len();
        let row_min_len = rows.min_by(|x, y| x.len().cmp(&y.len())).unwrap().len();

        if row_max_len != row_min_len {
            panic!("Row widths are inconsistent in the loaded map");
        }

        let valid_chars: &[_] = &['0', '1', '2', '3', '\n'];        
        if data.trim_matches(valid_chars).len() > 0 {
            panic!("Unknown characters found in the loaded map");   
        }

        if !data.contains('2') {
            panic!("Start not found in the loaded map");
        }

        if !data.contains('3') {
            panic!("End not found in the loaded map");
        }
    }

    pub fn from_file(filename: &'static str) -> Map {
        let data = fs::read_to_string(filename).expect("Unable to read file");
        Map::validate_map_raw_data(&data);
        let mut tiles = HashMap::new();
        let mut x: i32 = 0;
        let mut y: i32 = 0;
        for c in data.chars() {
            match c {
                '\n' => {
                    y += 1;
                    x = 0;
                    continue;
                },
                '0' => { tiles.insert((x, y), Tile::new(x, y, TileType::Solid)); },
                '1' => { tiles.insert((x, y), Tile::new(x, y, TileType::Space)); },
                '2' => { tiles.insert((x, y), Tile::new(x, y, TileType::Start)); },
                '3' => { tiles.insert((x, y), Tile::new(x, y, TileType::End)); },
                _ => {},
            }

            x += 1;
        }

        Map {
            tiles,
        }
    }

    pub fn modify_tile_type_at(&mut self, map_x: i32, map_y: i32, new_tile_type: TileType) {
        let mut test = false;
        if let Some(tile) = self.tile_at(map_x, map_y) {
            if tile.is_space() {
                test = true;
            }
        }        
        
        if test {
            self.tiles.insert((map_x, map_y), Tile::new(map_x, map_y, new_tile_type));
        }
    }    

    pub fn tile_at(&self, map_x: i32, map_y: i32) -> Option<&Tile> {
        if self.outside_bounds(map_x, map_y) {
            return None;
        }

        self.tiles.get(&(map_x, map_y))
    }

    pub fn outside_bounds(&self, map_x: i32, map_y: i32) -> bool {
        map_x < 0 || map_y < 0 || map_y >= MAP_TILES as i32 || map_x >= MAP_TILES as i32
    }

    pub fn start(&self) -> Option<Tile> {
        for tile in self.tiles.values() {
            if tile.is_start() {
                return Some(*tile);
            }
        }

        None
    }

    pub fn tile_neighbors(&self, tile: Tile) -> Vec<Tile> {
        let mut tiles = Vec::new();
        let x = tile.map_x;
        let y = tile.map_y;

        if y > 0 {
            if let Some(t) = self.tile_at(x, y - 1) {
                tiles.push(*t);                
            }
        }

        if y + 1 < MAP_TILES as i32 {
            if let Some(t) = self.tile_at(x, y + 1) {
                tiles.push(*t);                
            }
        }

        if x > 0 {
            if let Some(t) = self.tile_at(x - 1, y) {
                tiles.push(*t);                
            }
        }

        if x + 1 < MAP_TILES as i32 {
            if let Some(t) = self.tile_at(x + 1, y) {
                tiles.push(*t);                
            }
        }

        tiles
    }

    pub fn print(&self) {
        for y in 0..MAP_TILES as i32 {
            for x in 0..MAP_TILES as i32 {
                if let Some(tile) = self.tiles.get(&(x, y)) {
                    tile.print();
                }
            }

            println!("");
        }
    }
}
