use amethyst::audio::OggFormat;
use amethyst::winit::dpi::LogicalPosition;
use std::collections::HashMap;
use amethyst::audio::output::init_output;
use amethyst::winit::{ElementState, WindowEvent};
use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::nalgebra::{Matrix4, Vector3};
use amethyst::core::transform::{GlobalTransform, Transform};
use amethyst::ecs::prelude::{Entity};
use amethyst::input::{is_close_requested, is_key_down};
use amethyst::prelude::*;
use amethyst::renderer::{
    Camera, Event, PngFormat, Projection, Sprite, SpriteRender, SpriteSheet, SpriteSheetHandle, Texture, TextureMetadata, 
    TextureCoordinates, VirtualKeyCode, ScreenDimensions,
};
use amethyst::ui::{UiCreator, UiFinder, UiEventType};
use amethyst::shrev::{EventChannel};
use components::*;

use constants::*;
use map::{Map};

use path::{graph_to_path, map_to_graph};

pub struct GameResource {
    pub path: Vec<Tile>,
    pub mouse_pos: Rect,
    pub window: (f32, f32),
    pub map: Map,
    pub sprite_sheet_handle: Option<SpriteSheetHandle>,    
    pub selected_object: Option<PlaceableObjectType>,
    pub occupied_tiles: HashMap<(i32, i32), bool>,
}

impl GameResource {
    pub fn new(sprite_sheet_handle: Option<SpriteSheetHandle>) -> Self {
        let map = Map::from_file("lev.txt");
        let graph = map_to_graph(&map);
        let path = graph_to_path(&graph);

        GameResource {
            path,
            mouse_pos: Rect::new(0.0, 0.0, 1.0, 1.0),
            window: (1.0, 1.0),
            map,
            sprite_sheet_handle,            
            selected_object: None,            
            occupied_tiles: HashMap::new(),
        }
    }

    pub fn update_mouse_pos(&mut self, position: LogicalPosition) {
        self.mouse_pos.pos.x = position.x as f32;
        self.mouse_pos.pos.y = position.y as f32;
    }

    pub fn mouse_pos_to_map(&self) -> (i32, i32) {
        let tile_size_x = self.window.0 * RELATIVE_GAMEAREA_WIDTH / MAP_TILES as f32;
        let tile_size_y = self.window.1 / MAP_TILES as f32;
        let mouse_map_x = (self.mouse_pos.pos.x / tile_size_x) as i32;
        let mouse_map_y = (self.mouse_pos.pos.y / tile_size_y) as i32;        

        (mouse_map_x, mouse_map_y)
    }

    pub fn update_path(&mut self) {
        let graph = map_to_graph(&self.map);
        self.path = graph_to_path(&graph);
    }
}

impl Default for GameResource {
    fn default() -> GameResource {
        GameResource::new(None)
    }
}

pub fn gamearea_size() -> (f32, f32) {
    (MAP_TILES as f32 * TILE_SIZE, MAP_TILES as f32 * TILE_SIZE)
}

fn uiarea_size() -> (f32, f32) {
    let w = RELATIVE_UIAREA_WIDTH / RELATIVE_GAMEAREA_WIDTH * (MAP_TILES as f32 * TILE_SIZE);
    (w, MAP_TILES as f32 * TILE_SIZE)
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum PlaceableObjectType {
    Gun(usize), // The data is sprite number
    Dampener(usize),
    Tile(usize),
}

impl PlaceableObjectType {    
    pub fn sprite_number(&self) -> &usize {
        match self {
            PlaceableObjectType::Gun(sprite_number) => sprite_number,
            PlaceableObjectType::Dampener(sprite_number) => sprite_number,
            PlaceableObjectType::Tile(sprite_number) => sprite_number,
        }
    }
}

#[derive(Debug)]
pub enum GameEvent {
    SelectObject,
    SelectObjectClear,
    PlaceObject(i32, i32, PlaceableObjectType),    
}

pub struct Game {
    btn_object_map: HashMap<Entity, PlaceableObjectType>,
}

impl Game {
    pub fn new() -> Self {
        Self {
            btn_object_map: HashMap::new(),
        }
    }
}

impl SimpleState for Game {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
    // fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;

        let sprite_sheet_handle = load_sprite_sheet(world);

        let game_resource = GameResource::new(Some(sprite_sheet_handle.clone()));

        world.register::<Tile>();
        world.register::<Creep>();
        world.register::<Rect>();
        world.register::<Velocity>();
        world.register::<MoneyReward>();
        world.register::<Shooter>();        
        world.register::<Dampener>();        
        world.register::<Range>();        
        world.register::<Damage>();        
        world.register::<DealDamage>();        
        world.register::<Side>();        

        initialise_tiles(world, &game_resource.map, &sprite_sheet_handle);

        let channel = EventChannel::<GameEvent>::new();
        world.add_resource(game_resource); 
        world.add_resource(channel); 

        initialise_camera(world);

        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet_handle.clone(),
            sprite_number: 4, 
        };

        let rect = Rect::new_from_map_coords(0, 0, TILE_SIZE, TILE_SIZE);
        let mut transform = Transform::default();
        transform.set_position(rect.center());

        world
            .create_entity()
            .with(sprite_render)
            .with(rect)
            .with(transform)
            .with(Mouse)
            .build();

        init_output(&mut world.res);    
        world.exec(|mut creator: UiCreator| {
            creator.create("ui.ron", ());
        });

        let source_handle = {            
            let loader = world.read_resource::<Loader>();
            loader.load("audio/shot.ogg", OggFormat, (), (), &world.read_resource())
        };
        world.add_resource(source_handle);
    }

    fn handle_event(&mut self, data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> SimpleTrans {
        let mut game_resource = data.world.write_resource::<GameResource>();
        let mut game_ui_event_channel = data.world.write_resource::<EventChannel<GameEvent>>();

        match event {
            StateEvent::Window(event) => {
                match event {
                    Event::WindowEvent { event: WindowEvent::CursorMoved { position, .. }, .. } => {
                        (*game_resource).update_mouse_pos(position);                        
                    },
                    Event::WindowEvent { event: WindowEvent::MouseInput {state: ElementState::Released, ..}, ..} => {
                        if let Some(selected_object) = game_resource.selected_object { 
                            let (x, y) = game_resource.mouse_pos_to_map();

                            game_ui_event_channel.single_write(GameEvent::PlaceObject(x, y, selected_object));                                        
                        }
                    }
                    _ => ()
                }

                if is_key_down(&event, VirtualKeyCode::Escape) {
                    if game_resource.selected_object.is_some() {
                        game_resource.selected_object = None;
                        game_ui_event_channel.single_write(GameEvent::SelectObjectClear);
                    } else {
                        return Trans::Quit;
                    }
                }

                if is_close_requested(&event) {
                    Trans::Quit
                } else {
                    Trans::None
                }
            },
            StateEvent::Ui(event) => {  
                match event.event_type {
                    UiEventType::Click => {
                        if let Some(object_type) = self.btn_object_map.get(&event.target) {
                            game_resource.selected_object = Some(*object_type);
                            game_ui_event_channel.single_write(GameEvent::SelectObject);
                        }
                    }, 
                    _ => {}
                }
                Trans::None           
            }
        }
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        let StateData { world, data } = data;
        data.update(&world);
        
        {   
            let screen = world.read_resource::<ScreenDimensions>();
            let mut game_resource = world.write_resource::<GameResource>();
            game_resource.window.0 = screen.width();
            game_resource.window.1 = screen.height();
        }
                     
        world.exec(|finder: UiFinder| {
            // todo clean, this is run every frame
            if let Some(entity) = finder.find("gun") {                  
                self.btn_object_map.insert(entity, PlaceableObjectType::Gun(5));
            }

            if let Some(entity) = finder.find("dampener") {                  
                self.btn_object_map.insert(entity, PlaceableObjectType::Dampener(6));
            }

            if let Some(entity) = finder.find("tile") {                  
                self.btn_object_map.insert(entity, PlaceableObjectType::Tile(0));
            }
        });

        Trans::None
    }
}

fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/sprites.png",
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };

    let mut sprites: Vec<Sprite> = Vec::new();
    
    let cols = 5;
    let rows = 5;

    for y in 0..rows {
        for x in 0..cols {
            let offset_x = x as f32 / cols as f32;
            let offset_y = y as f32 / rows as f32;
            let tex_coords = TextureCoordinates {
                left: offset_x,
                right: offset_x + TILE_SIZE / SPRITESHEET_SIZE.0,
                top: 1.0 - (offset_y + TILE_SIZE / SPRITESHEET_SIZE.1),
                bottom: 1.0 - offset_y,
            };

            let size = match y {
                2 => (CREEP_SIZE, CREEP_SIZE),
                3 => (PROJECTILE_SIZE, PROJECTILE_SIZE),
                4 => match x {
                    0 => (CREEP_SIZE, 3.),
                    1 => (CREEP_SIZE, 3.),
                    _ => (TILE_SIZE, TILE_SIZE)
                }
                _ => (TILE_SIZE, TILE_SIZE)
            };

            let sprite = Sprite {
                width: size.0,
                height: size.1,
                offsets: [0.0, 0.0],
                tex_coords,
            };

            sprites.push(sprite);
        }
    }

    // Collate the sprite layout information into a sprite sheet
    let sprite_sheet = SpriteSheet {
        texture: texture_handle,
        sprites,
    };

    let sprite_sheet_handle = {
        let loader = world.read_resource::<Loader>();
        let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
        loader.load_from_data(sprite_sheet, (), &sprite_sheet_store)
    };

    sprite_sheet_handle
}

fn initialise_tiles(world: &mut World, map: &Map, sprite_sheet_handle: &SpriteSheetHandle) {
    for tile in map.tiles.values() {
        let mut transform = Transform::default();
        transform.set_position(tile.rect.center());

        let sprite_render = SpriteRender {
            sprite_sheet: sprite_sheet_handle.clone(),
            sprite_number: tile.sprite_number(), 
        };
            
        world
            .create_entity()
            .with(sprite_render)
            .with(transform)
            .build();
    }     
}

/// Initialise the camera.
fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_z(1000.0);
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            gamearea_size().0 + uiarea_size().0,
            gamearea_size().1.max(uiarea_size().1),
            0.0,
        )))        
        .with(transform)
        .build();
}
