use amethyst::core::Transform;
use amethyst::renderer::SpriteRender;
use amethyst::ecs::prelude::{System, WriteStorage, Entities, Write};
use components::*;
use constants::*;
use game::{GameResource, GameEvent, PlaceableObjectType};
use amethyst::shrev::{EventChannel, ReaderId};

pub struct PlaceObjectSystem {
    reader_id: Option<ReaderId<GameEvent>>,
}

impl PlaceObjectSystem {
    pub fn new() -> Self {
        Self {
            reader_id: None,
        }
    }
}

impl<'a> System<'a> for PlaceObjectSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, SpriteRender>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Rect>,
        WriteStorage<'a, Side>,
        WriteStorage<'a, DealDamage>,
        WriteStorage<'a, Shooter>,
        WriteStorage<'a, Dampener>,
        WriteStorage<'a, Range>,
        Write<'a, GameResource>,
        Write<'a, EventChannel<GameEvent>>,
    );

    fn run(&mut self, (
        entities, 
        mut sprite_render_storage, 
        mut transform_storage, 
        mut rect_storage, 
        mut side_storage,
        mut deal_damage_storage,
        mut shooter_storage,
        mut dampener_storage,
        mut range_storage,
        mut game_resource,
        mut game_ui_event_channel,
    ): Self::SystemData) {
        if self.reader_id.is_none() {
            self.reader_id = Some(game_ui_event_channel.register_reader());
        }
     
        for ev in game_ui_event_channel.read(self.reader_id.as_mut().unwrap()) {
            match ev {
                GameEvent::PlaceObject(click_map_x, click_map_y, object_type) => {
                    let sprite_sheet_handle = game_resource.sprite_sheet_handle.clone().unwrap();

                    match object_type {
                        PlaceableObjectType::Gun(_) | PlaceableObjectType::Dampener(_) => {
                            if game_resource.occupied_tiles.get(&(*click_map_x, *click_map_y)).is_some() {
                                continue;
                            }

                            if let Some(tile) = game_resource.map.tile_at(*click_map_x, *click_map_y) {
                                if !tile.is_solid() {
                                    continue;
                                }
                            }

                            if game_resource.map.outside_bounds(*click_map_x, *click_map_y) {
                                continue;
                            }

                            let gun_rect = Rect::new_from_map_coords(*click_map_x, *click_map_y, TILE_SIZE, TILE_SIZE);
                            let mut transform = Transform::default();
                            transform.set_position(gun_rect.center());

                            match object_type {
                                PlaceableObjectType::Gun(sprite_number) => { 
                                    let sprite_render = SpriteRender {
                                        sprite_sheet: sprite_sheet_handle.clone(),
                                        sprite_number: *sprite_number, 
                                    };

                                    let shooter = Shooter {
                                        time_since_shot: 1.0 / TURRET_ROF,
                                        shooting_direction: None,
                                        projectile_spawn_pos: (*click_map_x, *click_map_y)
                                    };

                                    entities.build_entity()            
                                        .with(sprite_render, &mut sprite_render_storage)
                                        .with(gun_rect, &mut rect_storage)
                                        .with(transform, &mut transform_storage)
                                        .with(Side::Good, &mut side_storage)
                                        .with(shooter, &mut shooter_storage)
                                        .with(DealDamage::new(5, vec![Side::Evil]), &mut deal_damage_storage)
                                        .with(Range::new(TURRET_RANGE), &mut range_storage)
                                        .build();                                
                                },
                                PlaceableObjectType::Dampener(sprite_number) => {
                                    let sprite_render = SpriteRender {
                                        sprite_sheet: sprite_sheet_handle.clone(),
                                        sprite_number: *sprite_number,
                                    };

                                    let dampener_rect = Rect::new_from_map_coords(*click_map_x, *click_map_y, TILE_SIZE, TILE_SIZE);
                                    let mut transform = Transform::default();
                                    transform.set_position(dampener_rect.center());

                                    entities
                                        .build_entity()
                                        .with(sprite_render, &mut sprite_render_storage)
                                        .with(dampener_rect, &mut rect_storage)
                                        .with(transform, &mut transform_storage)
                                        .with(Dampener::default(), &mut dampener_storage)
                                        .with(Range::new(DAMPENER_RANGE), &mut range_storage)
                                        .with(Side::Good, &mut side_storage)
                                        .build();   
                                },
                                _ => {}
                            }

                            game_resource.occupied_tiles.insert((*click_map_x, *click_map_y), true);
                        },
                        PlaceableObjectType::Tile(_) => {
                            game_resource.map.modify_tile_type_at(*click_map_x, *click_map_y, TileType::Solid);
                            game_resource.update_path();

                            if let Some(tile) = game_resource.map.tile_at(*click_map_x, *click_map_y) {
                                let sprite_render = SpriteRender {
                                    sprite_sheet: sprite_sheet_handle.clone(),
                                    sprite_number: tile.sprite_number(),
                                };

                                let mut transform = Transform::default();
                                transform.set_position(tile.rect.center());

                                entities
                                    .build_entity()
                                    .with(sprite_render, &mut sprite_render_storage)
                                    .with(transform, &mut transform_storage)
                                    .build();
                            }
                        }
                    }
                },
                _ => {}
            }
        }        
    }
}
