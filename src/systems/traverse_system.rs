use amethyst::core::Transform;
use amethyst::core::timing::Time;
use amethyst::ecs::prelude::{Join, Read, System, WriteStorage};
use components::*;
use game::GameResource;
use constants::*;

pub struct TraverseSystem;

impl<'a> System<'a> for TraverseSystem {
    type SystemData = (
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Creep>,
        WriteStorage<'a, Rect>,
        WriteStorage<'a, Transform>,
        Read<'a, GameResource>,
        Read<'a, Time>,
    );

    fn run(&mut self, (mut v, mut creep, mut creep_rect, mut transform, game_resource, time): Self::SystemData) {
        for (v, creep, creep_rect, transform) in (&mut v, &mut creep, &mut creep_rect, &mut transform).join() {
            // check where creep is at
            let (map_x, map_y) = creep_rect.to_map_coords();           
            let creep_tile = game_resource.map.tile_at(map_x, map_y);

            if let Some(tile) = creep_tile {                
                // calculate creep center distance from center of tile on this and next frame
                // not bulletproof method on high speeds but probably good enough for this game
                let d1 = (creep_rect.center() - tile.rect.center()).magnitude_squared();
                let d2 = ((creep_rect.center() + v.0 * TILE_SIZE * time.fixed_seconds()) - tile.rect.center()).magnitude_squared();
                let visited_tile_center = d2 > d1;

                // if the tile creep is at is not visited yet AND creep is almost perfectly aligned on top of the tile                  
                if !creep.visited.contains(tile) && visited_tile_center { 
                    creep.visited.push(*tile);
                // if reached the end, jump back to start
                } else if tile.is_end() && visited_tile_center {
                    let start = game_resource.map.start().unwrap();
                    *creep_rect = Rect::new_from_map_coords(start.map_x, start.map_y, CREEP_SIZE, CREEP_SIZE);
                    transform.set_position(creep_rect.center());
                    v.0.x = 0.0;
                    v.0.y = 0.0;
                    creep.spawn_time = CREEP_RESPAWN_TIME;
                    creep.visited.clear();
                    creep.visited.push(start);
                }   
            } 

            // set direction towards next unvisited tile
            for tile in &game_resource.path {
                if !creep.visited.contains(&tile) {
                    let dir = tile.rect.center() - creep_rect.center();
                    let uv = dir.normalize();
                    v.0 = uv * CREEP_SPEED;
                    break;
                }
            }
        }
    }
}
