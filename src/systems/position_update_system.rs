use amethyst::core::timing::Time;
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::{Entities, Join, Read, ReadStorage, System, WriteStorage};
use components::*;
use constants::*;

pub struct PositionUpdateSystem;

impl<'a> System<'a> for PositionUpdateSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Velocity>,
        ReadStorage<'a, Creep>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Rect>,
        WriteStorage<'a, Projectile>,
        Read<'a, Time>,        
    );

    fn run(&mut self, (entities, v, creeps, mut transforms, mut rects, mut projectiles, time): Self::SystemData) {
        for (ent, v, transform, rect) in (&entities, &v, &mut transforms, &mut rects).join() {    
            let creep: Option<&Creep> = creeps.get(ent); 
            if let Some(creep) = &creep {
                if creep.spawn_time > 0.0 {
                    continue;
                } 
            }   
                
            rect.pos += v.0 * TILE_SIZE * time.fixed_seconds();
            transform.set_position(rect.center());
        }

        for projectile in (&mut projectiles).join() {
            projectile.time_to_live -= time.fixed_seconds();
        }
    }
}
