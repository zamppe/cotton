use amethyst::ecs::prelude::{Join, ReadStorage, System, Entities};
use components::{Creep, Projectile};

pub struct CleanupSystem;

impl<'a> System<'a> for CleanupSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Projectile>,
        ReadStorage<'a, Creep>,
    );

    fn run(&mut self, (
        entities,
        projectiles,
        creep,
    ): Self::SystemData) {
        for (projectile_entity, projectile) in (&*entities, &projectiles).join() {
            // todo having ttl as component could be better?
            if projectile.time_to_live <= 0.0 {
                entities.delete(projectile_entity);
            }
        }

        for (creep_ent, creep) in (&*entities, &creep).join() {
            if creep.health <= 0 {
                entities.delete(creep_ent);
                for ent in (&entities).join() {
                    if ent.id() == creep.healthbar_id {
                        entities.delete(ent);
                    }
                }
            }
        }
    }
}
