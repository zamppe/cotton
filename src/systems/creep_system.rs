use std::collections::HashMap;
use amethyst::core::nalgebra::Vector3;
use amethyst::core::timing::Time;
use amethyst::ecs::prelude::{Join, Read, System, WriteStorage, Entities};
use amethyst::core::transform::{Transform};
use components::*;

pub struct CreepSystem;

impl<'a> System<'a> for CreepSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Creep>,     
        WriteStorage<'a, Transform>,     
        Read<'a, Time>,
    );

    fn run(&mut self, (entities, mut creeps, mut transforms, time): Self::SystemData) {
        // Map healthbar ids to (position of the creep, scaling vector)
        let mut map: HashMap<u32, (Vector3<f32>, Vector3<f32>)> = HashMap::new();        
        for (creep, transform) in (&mut creeps, &mut transforms).join() {
            if creep.spawn_time > 0.0 {
                creep.spawn_time -= time.fixed_seconds();
            }       
            map.insert(creep.healthbar_id, (*transform.translation(), Vector3::new(creep.health as f32 / creep.max_health as f32, 1.0, 1.0)));     
        }

        for ent in (&entities).join() {
            for id in map.keys() {
                if ent.id() == *id {
                    let mut transform = transforms.get_mut(ent).unwrap();
                    let value = *map.get(id).unwrap();
                    transform.set_position(value.0);
                    transform.translate_y(-10.0);
                    transform.set_scale(value.1.x, value.1.y, value.1.z);
                }
            }                
        } 
    }
}
