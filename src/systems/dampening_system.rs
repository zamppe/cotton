use amethyst::ecs::prelude::{Join, ReadStorage, System, WriteStorage, Entities, Entity};
use components::{Creep, Rect, Dampener, Range, Debuff};

pub struct DampeningSystem;

impl<'a> System<'a> for DampeningSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Creep>,
        ReadStorage<'a, Rect>,
        ReadStorage<'a, Dampener>,
        ReadStorage<'a, Range>,        
        WriteStorage<'a, Debuff>,
    );

    fn run(&mut self, (
        entities, 
        creeps, 
        rects, 
        dampeners,
        ranges,
        mut debuffs, 
    ): Self::SystemData) {
        let mut dampening_debuffs: Vec<Entity> = Vec::new();

        for (entity, _creep) in (&*entities, &creeps).join() {
            if let Some(debuff) = debuffs.get(entity) {
                match debuff {
                    Debuff::Dampening(_) => {
                        dampening_debuffs.push(entity);
                    },
                    _ => {}
                }
            }
        }

        for entity in dampening_debuffs.iter() {
            debuffs.remove(*entity);
        }

        for (dampener_rect, dampener, range) in (&rects, &dampeners, &ranges).join() {
            for (entity, _creep, creep_rect) in (&*entities, &creeps, &rects).join() {                
                if (*dampener_rect - *creep_rect).magnitude() < range.0 {
                    debuffs.insert(entity, Debuff::Dampening(dampener.slowing_effect));         
                }
            }
        }
    }
}
