use amethyst::core::nalgebra::Vector3;
use amethyst::ecs::prelude::{Join, ReadStorage, System, WriteStorage};
use components::*;

pub struct TargetingSystem;

impl<'a> System<'a> for TargetingSystem {
    type SystemData = (
        ReadStorage<'a, Creep>,
        ReadStorage<'a, Rect>,
        ReadStorage<'a, Range>,
        WriteStorage<'a, Shooter>,
    );

    fn run(&mut self, (
        creep, 
        rects,
        range,
        mut shooter, 
    ): Self::SystemData) {
        for (shooter_rect, shooter, range) in (&rects, &mut shooter, &range).join() {
            // find closest creep entity for this shooting entity
            let closest_target = {
                let mut min = 9999.0f32;
                let mut closest_target: Option<Vector3<f32>> = None;

                for (target_rect, creep) in (&rects, &creep).join() {
                    if creep.spawn_time <= 0.0 {
                        let dist = (shooter_rect.center() - target_rect.center()).magnitude();
                        if dist < min {
                            min = dist;
                            closest_target = Some(target_rect.center());
                        }                 
                    }
                }

                closest_target
            };

            shooter.shooting_direction = None;

            if let Some(closest_target) = closest_target {
                if (shooter_rect.center() - closest_target).magnitude() > range.0 {
                    continue;
                }
                
                shooter.shooting_direction = Some((closest_target - shooter_rect.center()).normalize());
            }
        }
    }
}
