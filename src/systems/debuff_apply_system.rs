use amethyst::ecs::prelude::{Join, ReadStorage, System, WriteStorage};
use components::*;

pub struct DebuffApplySystem;

impl<'a> System<'a> for DebuffApplySystem {
    type SystemData = (
        WriteStorage<'a, Velocity>,
        ReadStorage<'a, Debuff>,
    );

    fn run(&mut self, (mut v, debuffs): Self::SystemData) {
        for (v, debuff) in (&mut v, &debuffs).join() {    
            match debuff {
                Debuff::Dampening(dampening_effect) => {
                    v.0 *= *dampening_effect;
                }
            }
        }
    }
}
