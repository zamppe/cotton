use components::Shooter;
use amethyst::core::timing::Time;
use amethyst::ecs::prelude::{Join, Read, System, WriteStorage};

pub struct ReloadSystem;

impl<'a> System<'a> for ReloadSystem {
    type SystemData = (
        WriteStorage<'a, Shooter>,
        Read<'a, Time>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (mut shooter, time) = data;

        for shooter in (&mut shooter).join() {
            shooter.time_since_shot += time.fixed_seconds();
        }
    }
}
