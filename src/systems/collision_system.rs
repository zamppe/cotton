use amethyst::ecs::prelude::{Join, ReadStorage, System, WriteStorage, Entities};
use components::*;
use constants::*;
use utils::rect_intersect;

pub struct CollisionSystem;

impl<'a> System<'a> for CollisionSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Creep>,
        WriteStorage<'a, Projectile>,
        ReadStorage<'a, Rect>,
        ReadStorage<'a, Damage>,
        ReadStorage<'a, Side>,
    );

    fn run(&mut self, (
        entities, 
        mut creep_storage, 
        mut projectiles, 
        rects, 
        damage,
        side,
    ): Self::SystemData) {
        for (projectile_rect, damage, projectile) in (&rects, &damage, &mut projectiles).join() {
            for (ent, target_rect, side) in (&*entities, &rects, &side).join() {                
                // todo maybe use somekind of "damage event"
                if rect_intersect(*projectile_rect, *target_rect) 
                    && projectile.time_to_live > 0.0 
                    && damage.side_target.contains(side)
                    && !damage.ignore.contains(&ent.id())
                {
                    projectile.time_to_live -= PROJECTILE_TTL;

                    let creep: Option<&mut Creep> = creep_storage.get_mut(ent);
                    if let Some(creep) = creep {                
                        if creep.health > 0 {
                            creep.apply_damage(damage);                            
                        }
                    }
                }
            }
        }
    }
}
