use amethyst::assets::AssetStorage;
use amethyst::audio::Source;
use amethyst::audio::output::Output;
use amethyst::audio::SourceHandle;
use amethyst::core::Transform;
use amethyst::renderer::SpriteRender;
use amethyst::ecs::prelude::{Join, Read, ReadExpect, System, ReadStorage, WriteStorage, Entities};
use components::*;
use constants::*;
use game::GameResource;
use std::ops::Deref;

pub struct ShootSystem;

impl<'a> System<'a> for ShootSystem {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, DealDamage>,
        WriteStorage<'a, Shooter>,
        WriteStorage<'a, SpriteRender>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Rect>,
        WriteStorage<'a, Projectile>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Damage>,
        Read<'a, GameResource>,
        ReadExpect<'a, SourceHandle>,
        Option<Read<'a, Output>>,
        Read<'a, AssetStorage<Source>>,
    );

    fn run(&mut self, (
        entities, 
        deal_damage, 
        mut shooter, 
        mut sprite_render_storage, 
        mut transform_storage, 
        mut rect_storage, 
        mut projectile_storage, 
        mut velocity_storage,
        mut damage_storage,
        game_resource,
        source_handle,
        audio_output,
        storage,
    ): Self::SystemData) {
        let mut shoot = false;
        for (ent, shooter, deal_damage) in (&*entities, &mut shooter, &deal_damage).join() {
            // if shooting entity has not reloaded
            if shooter.time_since_shot < (1.0f32 / TURRET_ROF) {
                continue;
            }
            // if no target
            if shooter.shooting_direction.is_none() {
                continue;
            }

            if let Some(ref sprite_sheet_handle) = game_resource.sprite_sheet_handle {
                let sprite_render = SpriteRender {
                    sprite_sheet: sprite_sheet_handle.clone(),
                    sprite_number: 15,
                };

                let rect = Rect::new_from_map_coords(shooter.projectile_spawn_pos.0, shooter.projectile_spawn_pos.1, PROJECTILE_SIZE, PROJECTILE_SIZE);
                let mut transform = Transform::default();
                transform.set_position(rect.center());

                let mut ignore: Vec<u32> = Vec::new();
                ignore.push(ent.id());

                let damage = Damage::from(deal_damage, ignore);

                entities.build_entity()
                    .with(sprite_render, &mut sprite_render_storage)
                    .with(transform, &mut transform_storage)
                    .with(rect, &mut rect_storage)
                    .with(Projectile::default(), &mut projectile_storage)
                    .with(damage, &mut damage_storage)
                    .with(Velocity(shooter.shooting_direction.unwrap() * PROJECTILE_SPEED), &mut velocity_storage)
                    .build();

                shooter.time_since_shot = 0.0;
                shoot = true;
            }     
        }

        if shoot {            
            let output = audio_output.as_ref().map(|o| o.deref());
            if let Some(ref output) = output.as_ref() {
                if let Some(sound) = storage.get(&source_handle) {
                    output.play_once(sound, 1.0);
                }
            }             
        }
    }
}
