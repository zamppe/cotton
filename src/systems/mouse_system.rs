use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::{Join, Read, ReadStorage, System, WriteStorage, Write};
use amethyst::shrev::{EventChannel, ReaderId};
use amethyst::renderer::{SpriteRender};
use game::{GameResource, GameEvent, PlaceableObjectType};
use constants::*;
use components::{Mouse, Rect};

pub struct MouseSystem {
    reader_id: Option<ReaderId<GameEvent>>,
}

impl MouseSystem {
    pub fn new() -> Self {
        MouseSystem { reader_id: None }
    }
}

impl<'a> System<'a> for MouseSystem {
    type SystemData = (
        ReadStorage<'a, Mouse>,
        WriteStorage<'a, Rect>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, SpriteRender>,
        Read<'a, GameResource>,
        Write<'a, EventChannel<GameEvent>>,
    );

    fn run(&mut self, (mousecomponent, mut rects, mut transforms, mut sprites, game_resource, mut game_ui_event_channel): Self::SystemData) {
        if self.reader_id.is_none() {
            self.reader_id = Some(game_ui_event_channel.register_reader());
        }

        for (_mousecomponent, rect, sprite, transform) in (&mousecomponent, &mut rects, &mut sprites, &mut transforms).join() {          
            let (mouse_map_x, mouse_map_y) = game_resource.mouse_pos_to_map();

            *rect = Rect::new_from_map_coords(mouse_map_x, mouse_map_y, TILE_SIZE, TILE_SIZE);
            transform.set_position(rect.center());

            for ev in game_ui_event_channel.read(self.reader_id.as_mut().unwrap()) {
                match ev {
                    GameEvent::SelectObject => {
                        if let Some(selected_object) = game_resource.selected_object {
                            sprite.sprite_number = *selected_object.sprite_number();
                        }
                    }
                    GameEvent::SelectObjectClear => sprite.sprite_number = 4,
                    _ => {}
                }
            }
            
            transform.set_z(1001.0);
            if let Some(tile) = game_resource.map.tile_at(mouse_map_x, mouse_map_y) {
                if let Some(selected_object) = game_resource.selected_object {                    
                    match selected_object {
                        PlaceableObjectType::Tile(_) => {
                            if tile.is_space() {
                                transform.set_z(1.0);
                            }
                        },
                        _ => {
                            if tile.is_solid() {
                                transform.set_z(1.0);
                            }
                        }
                    }                
                }
            }            
        }
    }
}
