use amethyst::core::Transform;
use amethyst::renderer::SpriteRender;
use amethyst::ecs::prelude::{Join, Read, System, WriteStorage, Entities};
use components::*;
use game::GameResource;
use constants::*;
use LevelConfig;

pub struct WaveSpawnSystem {
    wave: u32,
}

impl WaveSpawnSystem {
    pub fn new(wave: u32) -> Self {
        Self {
            wave,
        }
    }
}

impl<'a> System<'a> for WaveSpawnSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, SpriteRender>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Rect>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, MoneyReward>,
        WriteStorage<'a, Creep>,
        WriteStorage<'a, Side>,
        Read<'a, GameResource>,
        Read<'a, LevelConfig>,
    );


    fn run(&mut self, (
        entities, 
        mut sprite_render_storage, 
        mut transform_storage, 
        mut rect_storage, 
        mut velocity_storage,
        mut money_reward_storage,
        mut creep_storage,
        mut side_storage,
        game_resource,
        level_config,
    ): Self::SystemData) {
        let LevelConfig { waves, health_multiplier} = *level_config;

        if self.wave > waves {
            return;
            // todo: all waves cleared, level completed
        }

        if (&mut creep_storage).join().count() > 0 {
            return;
        } else {
            self.wave += 1;
        }

        for i in 0..10 {      
            let spawn_time = (i as f32) / 2.0 + CREEP_RESPAWN_TIME;
            
            let v = Velocity::new(0.0, 0.0, 0.0);

            let path = &game_resource.path;
            let start = *path.first().unwrap();  

            let rect = Rect::new_from_map_coords(start.map_x, start.map_y, CREEP_SIZE, CREEP_SIZE);
            let sprite_sheet_handle =  game_resource.sprite_sheet_handle.clone().unwrap();

            // build healthbar
            let mut transform = Transform::default();
            transform.set_position(rect.center());
            transform.translate_y(10.0);

            let sprite_render = SpriteRender {
                sprite_sheet: sprite_sheet_handle.clone(),
                sprite_number: 21, 
            };

            let healthbar = entities.build_entity()
                .with(sprite_render, &mut sprite_render_storage)
                .with(transform, &mut transform_storage)
                .build();    

            // continue building creep
            let mut transform = Transform::default();
            transform.set_position(rect.center());

            let sprite_render = SpriteRender {
                sprite_sheet: sprite_sheet_handle.clone(),
                sprite_number: 10, 
            };

            let base_health = CREEP_HEALTH;
            let health = (base_health as f32 * health_multiplier.powf((self.wave - 1) as f32)) as u32;
            let mut creep = Creep::new(health, self.wave, healthbar.id(), spawn_time);
            creep.visited.push(start);

            entities.build_entity()
                .with(Side::Evil, &mut side_storage)
                .with(sprite_render, &mut sprite_render_storage)
                .with(transform, &mut transform_storage)
                .with(rect, &mut rect_storage)
                .with(v, &mut velocity_storage)
                .with(MoneyReward(CREEP_REWARD), &mut money_reward_storage)
                .with(creep, &mut creep_storage)
                .build();
        }
    }
}
