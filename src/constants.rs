pub const TILE_SIZE: f32 = 20.0;
pub const MAP_TILES: u32 = 16;
pub const RELATIVE_GAMEAREA_WIDTH: f32 = 0.8;
pub const RELATIVE_UIAREA_WIDTH: f32 = 0.2;
pub const SPRITESHEET_SIZE: (f32, f32) = (100.0, 100.0);

pub const CREEP_SIZE: f32 = 12.0;
pub const CREEP_SPEED: f32 = 2.0;
pub const CREEP_HEALTH: u32 = 1;
pub const CREEP_REWARD: u32 = 25;
pub const CREEP_RESPAWN_TIME: f32 = 2.0;

pub const TURRET_ROF: f32 = 0.5; // 1 shot per 2 seconds
pub const TURRET_COST: u32 = 50;
pub const TURRET_RANGE: f32 = 100.0;
pub const DAMPENER_COST: u32 = 150;
pub const DAMPENER_RANGE: f32 = 50.0;
pub const DAMPENER_SLOWING_EFFECT: f32 = 0.5;

pub const PROJECTILE_SIZE: f32 = 2.0;
pub const PROJECTILE_SPEED: f32 = 12.0; // squares per second
pub const PROJECTILE_TTL: f32 = 1.; // seconds

pub const INITIAL_MONEY: u32 = 100000;
pub const INITIAL_LIVES: u32 = 10;
