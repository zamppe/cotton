use amethyst::error::Error;
use amethyst::core::bundle::{SystemBundle};
use amethyst::ecs::prelude::{DispatcherBuilder};
use systems::*;

/// A bundle is a convenient way to initialise related resources, components and systems in a
/// world. 
pub struct GameBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for GameBundle {
    fn build(self, builder: &mut DispatcherBuilder<'a, 'b>) -> Result<(), Error> {
        builder.add(TraverseSystem, "traverse_system", &[]);
        builder.add(DampeningSystem, "dampening_system", &[]);
        builder.add(DebuffApplySystem, "debuff_apply_system", &[]);
        builder.add(MouseSystem::new(), "mouse_system", &[]);
        builder.add(PlaceObjectSystem::new(), "place_object_system", &[]);
        builder.add(ReloadSystem, "reload_system", &[]);
        builder.add(TargetingSystem, "targeting_system", &[]);
        builder.add(ShootSystem, "shoot_system", &[]);
        builder.add(CollisionSystem, "collision_system", &[]);
        builder.add(CleanupSystem, "cleanup_system", &[
            "collision_system",
        ]);
        builder.add(WaveSpawnSystem::new(0), "wavespawn_system", &[]);
        builder.add(PositionUpdateSystem, "position_update_system", &[
            "debuff_apply_system",
        ]);
        builder.add(CreepSystem, "creep_system", &[
            "position_update_system",
        ]);
        Ok(())
    }
}
