use constants::TILE_SIZE;
use components::Rect;

pub fn map_coord_to_space(map_coord: i32) -> f32 {
    (map_coord as f32) * TILE_SIZE
}

pub fn rect_intersect (rect1: Rect, rect2: Rect) -> bool {
    let x1 = rect1.pos.x;
    let x2 = rect2.pos.x;
    let y1 = rect1.pos.y;
    let y2 = rect2.pos.y;
    let w1 = rect1.br.x;
    let w2 = rect2.br.x;
    let h1 = rect1.br.y;
    let h2 = rect2.br.y;

    x1 < (x2 + w2) && (x1 + w1) > x2 && y1 < (y2 + h2) && (y1 + h1) > y2
}
