use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;

pub struct MoneyReward(pub u32);

impl Component for MoneyReward {
    type Storage = DenseVecStorage<Self>;
}
