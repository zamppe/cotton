use amethyst::ecs::prelude::{
    Component, 
    DenseVecStorage
};

pub enum Debuff {
    Dampening(f32),
}

impl Component for Debuff {
    type Storage = DenseVecStorage<Self>;
}
