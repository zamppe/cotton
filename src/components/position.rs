use amethyst::core::cgmath::Vector3;
use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;

// todo: maybe obsolete component, we will see
#[derive(Debug)]
pub struct Position(pub Vector3<f32>);

impl Position {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Position(Vector3::new(x, y, z))
    }
}

impl Component for Position {
    type Storage = DenseVecStorage<Self>;
}
