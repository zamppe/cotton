use amethyst::ecs::prelude::{
    Component, 
    DenseVecStorage
};

pub struct Range(pub f32);

impl Range {
    pub fn new(range: f32) -> Range {
        Range(range)
    }
}

impl Default for Range {
    fn default() -> Range {
        Range(0.0)
    }
}

impl Component for Range {
    type Storage = DenseVecStorage<Self>;
}
