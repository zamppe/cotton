use amethyst::ecs::prelude::NullStorage;
use amethyst::ecs::prelude::Component;

pub struct Mouse;

impl Default for Mouse {
    fn default() -> Mouse {
        Mouse
    }
}

impl Component for Mouse {
    type Storage = NullStorage<Self>;
}
