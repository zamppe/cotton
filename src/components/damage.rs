use amethyst::ecs::prelude::{
    Component, 
    DenseVecStorage
};
use components::Side;

/// side_target contains all alignments this component can damage
/// ignore can be used for ignoring self so turrets don't shoot themselves 
pub struct Damage {
    pub damage: u32,
    pub side_target: Vec<Side>,
    pub ignore: Vec<u32>, 
}

impl Damage {
    pub fn new(damage: u32, side_target: Vec<Side>, ignore: Vec<u32>) -> Self {
        Self {
            damage,
            side_target,
            ignore,
        }
    }

    pub fn from(deal_damage: &DealDamage, ignore: Vec<u32>) -> Self {
        let DealDamage {damage, ref side_target} = *deal_damage;

        Damage::new(damage, side_target.clone(), ignore)
    }
}

impl Default for Damage {
    fn default() -> Damage {
        let targets = vec![Side::Evil];        
        Damage::new(1, targets, Vec::new())  
    }
}

impl Component for Damage {
    type Storage = DenseVecStorage<Self>;
}

pub struct DealDamage {
    damage: u32,
    side_target: Vec<Side>,
}

impl DealDamage {
    pub fn new(damage: u32, side_target: Vec<Side>) -> Self {
        Self {
            damage,
            side_target,
        }
    }
}

impl Default for DealDamage {
    fn default() -> DealDamage {
        let targets = vec![Side::Evil];
        DealDamage::new(1, targets)        
    }
}

impl Component for DealDamage {
    type Storage = DenseVecStorage<Self>;
}
