use components::Rect;
use amethyst::ecs::prelude::{Component, DenseVecStorage};
use constants::*;

#[derive(Clone, Copy, Debug)]
pub enum TileType {
    Solid,
    Space,
    Start,
    End,
}

impl TileType {
    pub fn print(&self) {
        match self {
            TileType::Solid => print!("0"),
            TileType::Space => print!("1"),
            TileType::Start => print!("2"),
            TileType::End => print!("3"),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Tile {
    pub map_x: i32,
    pub map_y: i32,
    pub rect: Rect,
    pub tile_type: TileType,
}

#[allow(dead_code)]
impl Tile {
    pub fn new(map_x: i32, map_y: i32, tile_type: TileType) -> Tile {
        Tile {
            map_x,
            map_y,            
            rect: Rect::new_from_map_coords(map_x, map_y, TILE_SIZE, TILE_SIZE),
            tile_type,
        }
    }

    pub fn is_solid(&self) -> bool {
        match self.tile_type {
            TileType::Solid => true,
            _ => false,
        }
    }

    pub fn is_space(&self) -> bool {
        match self.tile_type {
            TileType::Space => true,
            _ => false,
        }
    }

    pub fn is_passable(&self) -> bool {
        match self.tile_type {
            TileType::Solid => false,
            _ => true,
        }
    }

    pub fn is_end(&self) -> bool {
        match self.tile_type {
            TileType::End => true,
            _ => false,
        }
    }

    pub fn is_start(&self) -> bool {
        match self.tile_type {
            TileType::Start => true,
            _ => false,
        }
    }

    pub fn print(&self) {
        self.tile_type.print();
    }

    pub fn sprite_number(&self) -> usize {
        match self.tile_type {
            TileType::Solid => 0,
            TileType::Space => 1,
            TileType::Start => 2,
            TileType::End => 3         
        }
    }
}

impl PartialEq for Tile {
    fn eq(&self, other: &Tile) -> bool {
        self.map_x == other.map_x && self.map_y == other.map_y
    }
}

impl Component for Tile {
    type Storage = DenseVecStorage<Self>;
}
