use amethyst::core::nalgebra::Vector3;
use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;

pub struct Velocity(pub Vector3<f32>);

impl Velocity {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Velocity(Vector3::new(x, y, z))
    }
}

impl Component for Velocity {
    type Storage = DenseVecStorage<Self>;
}
