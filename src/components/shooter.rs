use amethyst::core::nalgebra::Vector3;
use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;

pub struct Shooter {
    pub time_since_shot: f32,
    pub shooting_direction: Option<Vector3<f32>>,
    pub projectile_spawn_pos: (i32, i32),
}

impl Component for Shooter {
    type Storage = DenseVecStorage<Self>;
}
