use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;
use components::*;

pub struct Creep {
    pub health: u32,
    pub max_health: u32,
    pub wave: u32,
    pub healthbar_id: u32,
    pub visited: Vec<Tile>,
    pub spawn_time: f32,
}

impl Component for Creep {
    type Storage = DenseVecStorage<Self>;
}

impl Creep {
    pub fn new(health: u32, wave: u32, healthbar_id: u32, spawn_time: f32) -> Self {
        Self {
            health,
            max_health: health,
            wave,
            healthbar_id,
            visited: Vec::new(),
            spawn_time,
        }
    }

    pub fn apply_damage(&mut self, damage: &Damage) {
        if self.health > damage.damage {
            self.health -= damage.damage;
        } else {
            self.health = 0;
        }
    }
}
