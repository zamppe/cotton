use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;

#[derive(Clone, PartialEq)]
pub enum Side {
    Good,
    Evil,
}

impl Component for Side {
    type Storage = DenseVecStorage<Self>;
}
