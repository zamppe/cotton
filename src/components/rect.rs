use std::ops::Sub;
use amethyst::core::nalgebra::Vector3;
use amethyst::ecs::prelude::{Component, DenseVecStorage};
use constants::*;
use utils::map_coord_to_space;

#[derive(Clone, Copy, Debug)]
pub struct Rect {
    pub pos: Vector3<f32>,
    pub br: Vector3<f32>,
}

impl Rect {
    pub fn new(x1: f32, y1: f32, x2: f32, y2: f32) -> Rect {
        let mut rect = Rect {
            pos: Vector3::new(x1, y1, 0.0),
            br: Vector3::new(x2, y2, 0.0),
        };

        rect.move_to_tile_center();
        rect        
    } 

    pub fn new_from_map_coords(map_x: i32, map_y: i32, w: f32, h: f32) -> Rect {
        let x = map_coord_to_space(map_x);
        let y = map_coord_to_space(map_y);

        Rect::new(x, y, w, h)
    }

    pub fn move_to_tile_center(&mut self) {
        self.pos.x += (TILE_SIZE - self.width()) / 2.0;
        self.pos.y += (TILE_SIZE - self.height()) / 2.0;
    }    

    pub fn width(&self) -> f32 {
        self.br.x
    }

    pub fn height(&self) -> f32 {
        self.br.y
    }

    pub fn center(&self) -> Vector3<f32> {
        self.pos + self.br / 2.0
    }

    pub fn to_map_coords(&self) -> (i32, i32) {        
        let map_x = (self.center().x / TILE_SIZE) as i32;
        let map_y = (self.center().y / TILE_SIZE) as i32;

        (map_x, map_y)
    }
}

impl Sub for Rect {
    type Output = Vector3<f32>;
    fn sub(self, other: Rect) -> Vector3<f32> {
        self.center() - other.center()
    }
}

impl Component for Rect {
    type Storage = DenseVecStorage<Self>;
}
