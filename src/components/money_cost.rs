use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;

pub struct MoneyCost(pub u32);

impl Component for MoneyCost {
    type Storage = DenseVecStorage<Self>;
}
