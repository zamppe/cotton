use amethyst::ecs::prelude::{
    Component, 
    DenseVecStorage
};
use constants::*;

pub struct Dampener {
    pub slowing_effect: f32,
}

impl Default for Dampener {
    fn default() -> Dampener {
        Dampener {
            slowing_effect: DAMPENER_SLOWING_EFFECT
        }
    }
}

impl Component for Dampener {
    type Storage = DenseVecStorage<Self>;
}
