use amethyst::ecs::prelude::DenseVecStorage;
use amethyst::ecs::prelude::Component;
use constants::*;

pub struct Projectile {
    pub time_to_live: f32,
    pub damage: u32,
}

impl Default for Projectile {
    fn default() -> Projectile {
        Projectile {
            time_to_live: PROJECTILE_TTL,
            damage: 1,
        }
    }
}

impl Component for Projectile {
    type Storage = DenseVecStorage<Self>;
}
