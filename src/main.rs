extern crate amethyst;
extern crate petgraph;
#[macro_use]
extern crate serde;

mod constants;
mod systems;
mod game;
mod map;
mod bundles;
mod components;
mod path;
mod utils;

use amethyst::assets::{Processor};
use amethyst::audio::{AudioBundle, Source, SourceHandle};
use amethyst::config::Config;
use amethyst::utils::application_root_dir;
use amethyst::input::InputBundle;
use amethyst::core::{frame_limiter::FrameRateLimitStrategy, transform::TransformBundle};
use amethyst::prelude::*;
use amethyst::renderer::{DisplayConfig, DrawFlat2D, Pipeline, RenderBundle, Stage};
use amethyst::ui::{DrawUi, UiBundle};

use bundles::GameBundle;
use std::time::Duration;

use std::iter::Cycle;
use std::vec::IntoIter;

#[derive(Debug, Deserialize, Serialize)]
pub struct LevelConfig {
    waves: u32,
    health_multiplier: f32,
}

impl Default for LevelConfig {
    fn default() -> Self {
        LevelConfig {
            waves: 20,
            health_multiplier: 2.0,
        }
    }
}

pub struct Music {
    pub music: Cycle<IntoIter<SourceHandle>>,
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    use game::Game;

    let app_root = application_root_dir()?;

    let display_config_path = app_root.join("resources/display_config.ron");
    let config = DisplayConfig::load(&display_config_path);

    let level_path = app_root.join("assets/levels/level.ron");
    let level_config = LevelConfig::load(&level_path);    
    let assets_dir = format!("{}/assets/", env!("CARGO_MANIFEST_DIR"));

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawFlat2D::new())
            .with_pass(DrawUi::new()),
    );    

    let game_data = GameDataBuilder::default()
        // .with(Processor::<Source>::new(), "source_processor", &[])
        .with_bundle(AudioBundle::default())?
        .with_bundle(TransformBundle::new())?
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(InputBundle::<String, String>::new())?
        .with_bundle(UiBundle::<String, String>::new())?
        .with_bundle(GameBundle)?;
        // .with_basic_renderer(display_config_path, DrawShaded::<PosNormTex>::new(), true)?
        // .with(UiEventHandlerSystem::new(), "ui_event_handler", &[]);
    let mut game = Application::build(assets_dir, Game::new())?
        .with_resource(level_config)
        .with_frame_limit(
            FrameRateLimitStrategy::SleepAndYield(Duration::from_millis(5)),
            144,
        ).build(game_data)?;
    game.run();
    Ok(())
}
